tgitui
======

tgitui is a tiny terminal-based GIT user interface. It consists of a
single file, written in POSIX shell, and should work on most GNU/Linux
distributions.

It is simply a wrapper for the standard GIT commands.

I created it because I wanted frontends for `git stash` and `git log`,
with the possibility of showing contents of commits/stashes, without
having to copy/paste the commt/stash ID.

Warning
-------
This script has not been checked for possible security issues.
It could have bugs related to unescaped special characters and/or
bugs with `eval`.

Sub-commands
------------
* `add` - Interactively stage/unstage files from a list
* `commits` - Shows the log and allows the commits to be viewed
* `diff` - Shows diffs by file
* `stashed` - Shows stashed commits and allows viewing them

Sub-command names may be abbreviated as `a`/`c`/`d`/`s`

Each sub-command will present a list of files/commits/etc.
Press Up/Down/PageUp/PageDown to navigate, Enter to select or Q to quit.

System Requirements
-------------------
Most GNU/Linux distributions come with all of these pre-installed,
except for GIT:

* GIT
* xterm-compatible terminal or Linux console
  (control sequences are hard-coded for these)
* POSIX shell (with local variable support). For example `dash`, or `bash`
* GNU coreutils: `cut`, `dd`, `head`, `stty`
* `tput` (part of ncurses)

Known Bugs
----------
* Long commit messages or filenames messes up the display.
* The `add` subcommand only supports basic use cases (not merges etc.)
* The `add` subcommand could be improved with regards to usability.
* Cannot work with filenames with special characters
* Resizing the terminal window will mess up the output.
  Press Ctrl+L to refresh the contents.

See Also
--------
* [shedit](https://github.com/samuellb/shedit/) - An earlier, and mostly
  useless, project in POSIX shell. It is a *very* basic editor. Probably
  not very useful in practice, but the README of that projects explains
  some of the implementation techniques/hacks that are used in both
  `shedit` and in `tgitui`.
